<?php

/**
 * Remove boxes from Dashboard
 */
function twd_admin_hider_remove_dashboard_widgets() {
    $options = get_option( 'twd_admin_hider_dashboard_settings' );

    if ( isset( $options['dashboard_browser_nag'] ) && $options['dashboard_browser_nag'] == '1' )
        remove_meta_box( 'dashboard_browser_nag', 'dashboard', 'normal' );

    if ( isset( $options['dashboard_right_now'] ) && $options['dashboard_right_now'] == '1' )
        remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );

    if ( isset( $options['dashboard_recent_comments'] ) && $options['dashboard_recent_comments'] == '1' )
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );

    if ( isset( $options['dashboard_incoming_links'] ) && $options['dashboard_incoming_links'] == '1' )
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );

    if ( isset( $options['dashboard_plugins'] ) && $options['dashboard_plugins'] == '1' )
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );

    if ( isset( $options['dashboard_quick_press'] ) && $options['dashboard_quick_press'] == '1' )
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );

    if ( isset( $options['dashboard_recent_drafts'] ) && $options['dashboard_recent_drafts'] == '1' )
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );

    if ( isset( $options['dashboard_primary'] ) && $options['dashboard_primary'] == '1' )
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );

    if ( isset( $options['dashboard_secondary'] ) && $options['dashboard_secondary'] == '1' )
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
}
add_action( 'wp_dashboard_setup', 'twd_admin_hider_remove_dashboard_widgets' );


/**
 * Remove welcome panel
 */
$options = get_option( 'twd_admin_hider_dashboard_settings' );
if ( isset( $options['dashboard_welcome_panel'] ) && $options['dashboard_welcome_panel'] == '1' )
    remove_action( 'welcome_panel', 'wp_welcome_panel' );


/**
 * Remove meta boxes from default posts screen
 */
function twd_admin_hider_remove_default_post_metaboxes() {
    $options = get_option( 'twd_admin_hider_post_settings' );

    if ( isset( $options['post_postcustom'] ) && $options['post_postcustom'] == '1' )
        remove_meta_box( 'postcustom','post','normal' ); // Custom Fields Metabox

    if ( isset( $options['post_postexcerpt'] ) && $options['post_postexcerpt'] == '1' )
        remove_meta_box( 'postexcerpt','post','normal' ); // Excerpt Metabox

    if ( isset( $options['post_commentstatusdiv'] ) && $options['post_commentstatusdiv'] == '1' )
        remove_meta_box( 'commentstatusdiv','post','normal' ); // Discussion Metabox

    if ( isset( $options['post_commentsdiv'] ) && $options['post_commentsdiv'] == '1' )
        remove_meta_box( 'commentsdiv','post','normal' ); // Comments Metabox

    if ( isset( $options['post_trackbacksdiv'] ) && $options['post_trackbacksdiv'] == '1' )
        remove_meta_box( 'trackbacksdiv','post','normal' ); // Talkback Metabox

    if ( isset( $options['post_slugdiv'] ) && $options['post_slugdiv'] == '1' )
        remove_meta_box( 'slugdiv','post','normal' ); // Slug Metabox

    if ( isset( $options['post_authordiv'] ) && $options['post_authordiv'] == '1' )
        remove_meta_box( 'authordiv','post','normal' ); // Author Metabox
}
add_action( 'admin_menu','twd_admin_hider_remove_default_post_metaboxes' );


/**
 * Remove meta boxes from default pages screen
 */
function twd_admin_hider_remove_default_page_metaboxes() {
    $options = get_option( 'twd_admin_hider_page_settings' );

    if ( isset( $options['page_postcustom'] ) && $options['page_postcustom'] == '1' )
        remove_meta_box( 'postcustom','page','normal' ); // Custom Fields Metabox

    if ( isset( $options['page_postexcerpt'] ) && $options['page_postexcerpt'] == '1' )
        remove_meta_box( 'postexcerpt','page','normal' ); // Excerpt Metabox

    if ( isset( $options['page_commentstatusdiv'] ) && $options['page_commentstatusdiv'] == '1' )
        remove_meta_box( 'commentstatusdiv','page','normal' ); // Discussion Metabox

    if ( isset( $options['page_commentsdiv'] ) && $options['page_commentsdiv'] == '1' )
        remove_meta_box( 'commentsdiv','page','normal' ); // Comments Metabox

    if ( isset( $options['page_trackbacksdiv'] ) && $options['page_trackbacksdiv'] == '1' )
        remove_meta_box( 'trackbacksdiv','page','normal' ); // Talkback Metabox

    if ( isset( $options['page_slugdiv'] ) && $options['page_slugdiv'] == '1' )
        remove_meta_box( 'slugdiv','page','normal' ); // Slug Metabox

    if ( isset( $options['page_authordiv'] ) && $options['page_authordiv'] == '1' )
        remove_meta_box( 'authordiv','page','normal' ); // Author Metabox
}
add_action( 'admin_menu','twd_admin_hider_remove_default_page_metaboxes' );


/**
 * Remove menus from Admin
 */
function twd_admin_hider_remove_menus() {
    global $submenu;
    global $user_login;

    $options = get_option( 'twd_admin_hider_menu_settings' );

    if ( isset( $options['menu_posts'] ) && $options['menu_posts'] == '1' )
        remove_menu_page( 'edit.php' ); // Posts

    if ( isset( $options['menu_media'] ) && $options['menu_media'] == '1' )
        remove_menu_page( 'upload.php' ); // Media

    if ( isset( $options['menu_links'] ) && $options['menu_links'] == '1' )
        remove_menu_page( 'link-manager.php' ); // Links

    if ( isset( $options['menu_comments'] ) && $options['menu_comments'] == '1' )
        remove_menu_page( 'edit-comments.php' ); // Comments

    if ( isset( $options['menu_pages'] ) && $options['menu_pages'] == '1' )
        remove_menu_page( 'edit.php?post_type=page' ); // Pages

    $except = (array) apply_filters( 'twd_admin_hider_exceptions', array() );
    if ( ! in_array( $user_login, $except ) ) {
        if ( isset( $options['menu_dashboard_updates'] ) && $options['menu_dashboard_updates'] == '1' )
            remove_submenu_page ( 'index.php', 'update-core.php' ); // Dashboard-->Updates


        if ( isset( $options['menu_appearance'] ) && $options['menu_appearance'] == '1' )
            remove_menu_page( 'themes.php' ); // Appearance

        if ( isset( $options['menu_appearance_themes'] ) && $options['menu_appearance_themes'] == '1' )
            remove_submenu_page ( 'themes.php', 'themes.php' ); // Appearance-->Themes

        if ( isset( $options['menu_appearance_widgets'] ) && $options['menu_appearance_widgets'] == '1' )
            remove_submenu_page ( 'themes.php', 'widgets.php' ); // Appearance-->Widgets

        if ( isset( $options['menu_appearance_editor'] ) && $options['menu_appearance_editor'] == '1' )
            remove_submenu_page ( 'themes.php', 'theme-editor.php' ); // Appearance-->Editor


        if ( isset( $options['menu_plugins'] ) && $options['menu_plugins'] == '1' )
            remove_menu_page( 'plugins.php' ); // Plugins


        if ( isset( $options['menu_users'] ) && $options['menu_users'] == '1' )
            remove_menu_page( 'users.php' ); // Users


        if ( isset( $options['menu_tools'] ) && $options['menu_tools'] == '1' )
            remove_menu_page( 'tools.php' ); // Tools


        if ( isset( $options['menu_settings'] ) && $options['menu_settings'] == '1' )
            remove_menu_page( 'options-general.php' ); // Settings

        if ( isset( $options['menu_settings_general'] ) && $options['menu_settings_general'] == '1' )
            remove_submenu_page ( 'options-general.php', 'options-general.php' ); // Settings-->General

        if ( isset( $options['menu_settings_writing'] ) && $options['menu_settings_writing'] == '1' )
            remove_submenu_page ( 'options-general.php', 'options-writing.php' ); // Settings-->Writing

        if ( isset( $options['menu_settings_reading'] ) && $options['menu_settings_reading'] == '1' )
            remove_submenu_page ( 'options-general.php', 'options-reading.php' ); // Settings-->Reading

        if ( isset( $options['menu_settings_discussion'] ) && $options['menu_settings_discussion'] == '1' )
            remove_submenu_page ( 'options-general.php', 'options-discussion.php' ); // Settings-->Discussion

        if ( isset( $options['menu_settings_media'] ) && $options['menu_settings_media'] == '1' )
            remove_submenu_page ( 'options-general.php', 'options-media.php' ); // Settings-->Media

        if ( isset( $options['menu_settings_permalinks'] ) && $options['menu_settings_permalinks'] == '1' )
            remove_submenu_page ( 'options-general.php', 'options-permalink.php' ); // Settings-->Permalinks
    }
}
add_action( 'admin_menu', 'twd_admin_hider_remove_menus', 102 );
