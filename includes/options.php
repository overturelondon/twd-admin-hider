<?php

/**
 * Add Settings Page
 */
function twd_admin_hider_settings_add_page() {
    add_options_page(
        'TWD Admin Hider Settings',
        'TWD Admin Hider',
        'edit_theme_options',
        'twd_admin_hider_settings',
        'twd_admin_hider_settings_page'
    );
}
add_action( 'admin_menu', 'twd_admin_hider_settings_add_page' );


/**
 * Settings Page Content
 */
function twd_admin_hider_settings_page() {
?>
    <div class="wrap">
        <h1>TWD Admin Hider</h1>

        <?php $active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'dashboard_meta_boxes'; ?>

        <h2 class="nav-tab-wrapper">
            <a href="?page=twd_admin_hider_settings&amp;tab=dashboard_meta_boxes" class="nav-tab <?php echo $active_tab == 'dashboard_meta_boxes' ? 'nav-tab-active' : ''; ?>"><?php esc_html_e( 'Dashboard Meta Boxes', 'twd' ); ?></a>
            <a href="?page=twd_admin_hider_settings&amp;tab=post_meta_boxes" class="nav-tab <?php echo $active_tab == 'post_meta_boxes' ? 'nav-tab-active' : ''; ?>"><?php esc_html_e( 'Post Meta Boxes', 'twd' ); ?></a>
            <a href="?page=twd_admin_hider_settings&amp;tab=page_meta_boxes" class="nav-tab <?php echo $active_tab == 'page_meta_boxes' ? 'nav-tab-active' : ''; ?>"><?php esc_html_e( 'Page Meta Boxes', 'twd' ); ?></a>
            <a href="?page=twd_admin_hider_settings&amp;tab=menus" class="nav-tab <?php echo $active_tab == 'menus' ? 'nav-tab-active' : ''; ?>"><?php esc_html_e( 'Menus', 'twd' ); ?></a>
        </h2>

        <form method="post" action="options.php">
            <?php
                if ( $active_tab == 'dashboard_meta_boxes' ) {

                    settings_fields( 'twd_admin_hider_dashboard_settings' );      // Output the hidden fields, nonce, etc
                    do_settings_sections( 'twd_admin_hider_dashboard_settings' ); // Output the settings sections

                } elseif ( $active_tab == 'post_meta_boxes' ) {

                    settings_fields( 'twd_admin_hider_post_settings' );           // Output the hidden fields, nonce, etc
                    do_settings_sections( 'twd_admin_hider_post_settings' );      // Output the settings sections

                } elseif ( $active_tab == 'page_meta_boxes' ) {

                    settings_fields( 'twd_admin_hider_page_settings' );           // Output the hidden fields, nonce, etc
                    do_settings_sections( 'twd_admin_hider_page_settings' );      // Output the settings sections

                } else {

                    settings_fields( 'twd_admin_hider_menu_settings' );           // Output the hidden fields, nonce, etc
                    do_settings_sections( 'twd_admin_hider_menu_settings' );      // Output the settings sections

                }

                submit_button();
            ?>
        </form>
    </div>

<?php
}

/**
 * Settings Initilisation
 */
function twd_admin_hider_dashboard_settings_init() {

    if ( get_option( 'twd_admin_hider_dashboard_settings' ) == false ) {
        add_option( 'twd_admin_hider_dashboard_settings' );
    }

    /**
     * Meta Boxes - Dashboard
     */
    add_settings_section(
        'twd_admin_hider_settings_section_dashboard',
        'Meta Boxes - Dashboard',
        'twd_admin_hider_settings_section_dashboard_callback',
        'twd_admin_hider_dashboard_settings'
    );

        // Browser Nag
        add_settings_field(
            'dashboard_browser_nag',
            'Browser Nag',
            'dashboard_browser_nag_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Right Now
        add_settings_field(
            'dashboard_right_now',
            'Right Now',
            'dashboard_right_now_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Recent Comments
        add_settings_field(
            'dashboard_recent_comments',
            'Recent Comments',
            'dashboard_recent_comments_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Incoming Lings
        add_settings_field(
            'dashboard_incoming_links',
            'Incoming Links',
            'dashboard_incoming_links_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Plugins
        add_settings_field(
            'dashboard_plugins',
            'Plugins',
            'dashboard_plugins_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Quick Press
        add_settings_field(
            'dashboard_quick_press',
            'Quick Press',
            'dashboard_quick_press_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Recent Drafts
        add_settings_field(
            'dashboard_recent_drafts',
            'Recent Drafts',
            'dashboard_recent_drafts_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Dashboard Primary
        add_settings_field(
            'dashboard_primary',
            'Dashboard Primary',
            'dashboard_primary_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Dashboard Secondary
        add_settings_field(
            'dashboard_secondary',
            'Dashboard Secondary',
            'dashboard_secondary_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        // Welcome Panel
        add_settings_field(
            'dashboard_welcome_panel',
            'Welcome Panel',
            'dashboard_welcome_panel_callback',
            'twd_admin_hider_dashboard_settings',
            'twd_admin_hider_settings_section_dashboard'
        );

        register_setting( 'twd_admin_hider_dashboard_settings', 'twd_admin_hider_dashboard_settings' );
}
add_action( 'admin_init', 'twd_admin_hider_dashboard_settings_init' );


function twd_admin_hider_post_settings_init() {

    if ( get_option( 'twd_admin_hider_post_settings' ) == false ) {
        add_option( 'twd_admin_hider_post_settings' );
    }

   /**
    * Meta Boxes - Post
    */
    add_settings_section(
        'twd_admin_hider_settings_section_post',
        'Meta Boxes - Post',
        'twd_admin_hider_settings_section_post_callback',
        'twd_admin_hider_post_settings'
    );

        // Custom Fields
        add_settings_field(
            'post_postcustom',
            'Custom Fields',
            'post_postcustom_callback',
            'twd_admin_hider_post_settings',
            'twd_admin_hider_settings_section_post'
        );

        // Excerpt
        add_settings_field(
            'post_postexcerpt',
            'Excerpt',
            'post_postexcerpt_callback',
            'twd_admin_hider_post_settings',
            'twd_admin_hider_settings_section_post'
        );

        // Comment Status
        add_settings_field(
            'post_commentstatusdiv',
            'Comment Status',
            'post_commentstatusdiv_callback',
            'twd_admin_hider_post_settings',
            'twd_admin_hider_settings_section_post'
        );

        // Comments
        add_settings_field(
            'post_commentsdiv',
            'Comments',
            'post_commentsdiv_callback',
            'twd_admin_hider_post_settings',
            'twd_admin_hider_settings_section_post'
        );

        // Trackbacks
        add_settings_field(
            'post_trackbacksdiv',
            'Trackbacks',
            'post_trackbacksdiv_callback',
            'twd_admin_hider_post_settings',
            'twd_admin_hider_settings_section_post'
        );

        // Slug
        add_settings_field(
            'post_slugdiv',
            'Slug',
            'post_slugdiv_callback',
            'twd_admin_hider_post_settings',
            'twd_admin_hider_settings_section_post'
        );

        // Author
        add_settings_field(
            'post_authordiv',
            'Author',
            'post_authordiv_callback',
            'twd_admin_hider_post_settings',
            'twd_admin_hider_settings_section_post'
        );

        register_setting( 'twd_admin_hider_post_settings', 'twd_admin_hider_post_settings' );
}
add_action( 'admin_init', 'twd_admin_hider_post_settings_init' );


function twd_admin_hider_page_settings_init() {

    if ( get_option( 'twd_admin_hider_page_settings' ) == false ) {
        add_option( 'twd_admin_hider_page_settings' );
    }

    /**
     * Meta Boxes - Page
     */
    add_settings_section(
        'twd_admin_hider_settings_section_page',
        'Meta Boxes - Page',
        'twd_admin_hider_settings_section_page_callback',
        'twd_admin_hider_page_settings'
    );

        // Custom Fields
        add_settings_field(
            'page_postcustom',
            'Custom Fields',
            'page_postcustom_callback',
            'twd_admin_hider_page_settings',
            'twd_admin_hider_settings_section_page'
        );

        // Excerpt
        add_settings_field(
            'page_postexcerpt',
            'Excerpt',
            'page_postexcerpt_callback',
            'twd_admin_hider_page_settings',
            'twd_admin_hider_settings_section_page'
        );

        // Comment Status
        add_settings_field(
            'page_commentstatusdiv',
            'Comment Status',
            'page_commentstatusdiv_callback',
            'twd_admin_hider_page_settings',
            'twd_admin_hider_settings_section_page'
        );

        // Comments
        add_settings_field(
            'page_commentsdiv',
            'Comments',
            'page_commentsdiv_callback',
            'twd_admin_hider_page_settings',
            'twd_admin_hider_settings_section_page'
        );

        // Trackbacks
        add_settings_field(
            'page_trackbacksdiv',
            'Trackbacks',
            'page_trackbacksdiv_callback',
            'twd_admin_hider_page_settings',
            'twd_admin_hider_settings_section_page'
        );

        // Slug
        add_settings_field(
            'page_slugdiv',
            'Slug',
            'page_slugdiv_callback',
            'twd_admin_hider_page_settings',
            'twd_admin_hider_settings_section_page'
        );

        // Author
        add_settings_field(
            'page_authordiv',
            'Author',
            'page_authordiv_callback',
            'twd_admin_hider_page_settings',
            'twd_admin_hider_settings_section_page'
        );

        register_setting( 'twd_admin_hider_page_settings', 'twd_admin_hider_page_settings' );
}
add_action( 'admin_init', 'twd_admin_hider_page_settings_init' );


function twd_admin_hider_menu_settings_init() {

    if ( get_option( 'twd_admin_hider_menu_settings' ) == false ) {
        add_option( 'twd_admin_hider_menu_settings' );
    }

    /**
     * Menus
     */
    add_settings_section(
        'twd_admin_hider_settings_section_menus',
        'Menus',
        'twd_admin_hider_settings_section_menus_callback',
        'twd_admin_hider_menu_settings'
    );

        // Dashboard-->Updates
        add_settings_field(
            'menu_dashboard_updates',
            'Dashboard-->Updates',
            'menu_dashboard_updates_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Posts
        add_settings_field(
            'menu_posts',
            'Posts',
            'menu_posts_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Media
        add_settings_field(
            'menu_media',
            'Media',
            'menu_media_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Links
        add_settings_field(
            'menu_links',
            'Links',
            'menu_links_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Comments
        add_settings_field(
            'menu_comments',
            'Comments',
            'menu_comments_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Pages
        add_settings_field(
            'menu_pages',
            'Pages',
            'menu_pages_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Plugins
        add_settings_field(
            'menu_plugins',
            'Plugins',
            'menu_plugins_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Users
        add_settings_field(
            'menu_users',
            'Users',
            'menu_users_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Tools
        add_settings_field(
            'menu_tools',
            'Tools',
            'menu_tools_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Appearance
        add_settings_field(
            'menu_appearance',
            'Appearance',
            'menu_appearance_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Appearance-->Themes
        add_settings_field(
            'menu_appearance_themes',
            'Appearance-->Themes',
            'menu_appearance_themes_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Appearance-->Widgets
        add_settings_field(
            'menu_appearance_widgets',
            'Appearance-->Widgets',
            'menu_appearance_widgets_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Appearance-->Editor
        add_settings_field(
            'menu_appearance_editor',
            'Appearance-->Editor',
            'menu_appearance_editor_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Settings
        add_settings_field(
            'menu_settings',
            'Settings',
            'menu_settings_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Settings->General
        add_settings_field(
            'menu_settings_general',
            'Settings-->General',
            'menu_settings_general_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Settings->Writing
        add_settings_field(
            'menu_settings_writing',
            'Settings-->Writing',
            'menu_settings_writing_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Settings-->Reading
        add_settings_field(
            'menu_settings_reading',
            'Settings-->Reading',
            'menu_settings_reading_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Settings-->Discussion
        add_settings_field(
            'menu_settings_discussion',
            'Settings-->Discussion',
            'menu_settings_discussion_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Settings-->Media
        add_settings_field(
            'menu_settings_media',
            'Settings-->Media',
            'menu_settings_media_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        // Settings-->Permalinks
        add_settings_field(
            'menu_settings_permalinks',
            'Settings-->Permalinks',
            'menu_settings_permalinks_callback',
            'twd_admin_hider_menu_settings',
            'twd_admin_hider_settings_section_menus'
        );

        register_setting( 'twd_admin_hider_menu_settings', 'twd_admin_hider_menu_settings' );
};
add_action( 'admin_init', 'twd_admin_hider_menu_settings_init' );


/**
 * Meta Boxes - Dashboard - Callbacks
 */

function twd_admin_hider_settings_section_dashboard_callback() {
    echo '<p>Select which Dashboard Meta Boxes you wish to hide.</p>';
}
    function dashboard_browser_nag_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_browser_nag'] ) ? checked( 1, $options['dashboard_browser_nag'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_browser_nag" name="twd_admin_hider_dashboard_settings[dashboard_browser_nag]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_right_now_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_right_now'] ) ? checked( 1, $options['dashboard_right_now'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_right_now" name="twd_admin_hider_dashboard_settings[dashboard_right_now]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_recent_comments_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_recent_comments'] ) ? checked( 1, $options['dashboard_recent_comments'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_recent_comments" name="twd_admin_hider_dashboard_settings[dashboard_recent_comments]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_incoming_links_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_incoming_links'] ) ? checked( 1, $options['dashboard_incoming_links'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_incoming_links" name="twd_admin_hider_dashboard_settings[dashboard_incoming_links]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_plugins_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_plugins'] ) ? checked( 1, $options['dashboard_plugins'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_plugins" name="twd_admin_hider_dashboard_settings[dashboard_plugins]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_quick_press_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_quick_press'] ) ? checked( 1, $options['dashboard_quick_press'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_quick_press" name="twd_admin_hider_dashboard_settings[dashboard_quick_press]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_recent_drafts_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_recent_drafts'] ) ? checked( 1, $options['dashboard_recent_drafts'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_recent_drafts" name="twd_admin_hider_dashboard_settings[dashboard_recent_drafts]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_primary_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_primary'] ) ? checked( 1, $options['dashboard_primary'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_primary" name="twd_admin_hider_dashboard_settings[dashboard_primary]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_secondary_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_secondary'] ) ? checked( 1, $options['dashboard_secondary'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_secondary" name="twd_admin_hider_dashboard_settings[dashboard_secondary]" value="1"' . $status . '/>';

        echo $html;
    }
    function dashboard_welcome_panel_callback() {
        $options = get_option( 'twd_admin_hider_dashboard_settings' );
        $status  = isset( $options['dashboard_welcome_panel'] ) ? checked( 1, $options['dashboard_welcome_panel'], false ) : '';

        $html    = '<input type="checkbox" id="dashboard_welcome_panel" name="twd_admin_hider_dashboard_settings[dashboard_welcome_panel]" value="1"' . $status . '/>';

        echo $html;
    }


/**
 * Meta Boxes - Post - Callbacks
 */

function twd_admin_hider_settings_section_post_callback() {
    echo '<p>Select which Post Meta Boxes you wish to hide.</p>';
}
    function post_postcustom_callback() {
        $options = get_option( 'twd_admin_hider_post_settings' );
        $status  = isset( $options['post_postcustom'] ) ? checked( 1, $options['post_postcustom'], false ) : '';

        $html    = '<input type="checkbox" id="post_postcustom" name="twd_admin_hider_post_settings[post_postcustom]" value="1"' . $status . '/>';

        echo $html;
    }
    function post_postexcerpt_callback() {
        $options = get_option( 'twd_admin_hider_post_settings' );
        $status  = isset( $options['post_postexcerpt'] ) ? checked( 1, $options['post_postexcerpt'], false ) : '';

        $html    = '<input type="checkbox" id="post_postexcerpt" name="twd_admin_hider_post_settings[post_postexcerpt]" value="1"' . $status . '/>';

        echo $html;
    }
    function post_commentstatusdiv_callback() {
        $options = get_option( 'twd_admin_hider_post_settings' );
        $status  = isset( $options['post_commentstatusdiv'] ) ? checked( 1, $options['post_commentstatusdiv'], false ) : '';

        $html    = '<input type="checkbox" id="post_commentstatusdiv" name="twd_admin_hider_post_settings[post_commentstatusdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function post_commentsdiv_callback() {
        $options = get_option( 'twd_admin_hider_post_settings' );
        $status  = isset( $options['post_commentsdiv'] ) ? checked( 1, $options['post_commentsdiv'], false ) : '';

        $html    = '<input type="checkbox" id="post_commentsdiv" name="twd_admin_hider_post_settings[post_commentsdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function post_trackbacksdiv_callback() {
        $options = get_option( 'twd_admin_hider_post_settings' );
        $status  = isset( $options['post_trackbacksdiv'] ) ? checked( 1, $options['post_trackbacksdiv'], false ) : '';

        $html    = '<input type="checkbox" id="post_trackbacksdiv" name="twd_admin_hider_post_settings[post_trackbacksdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function post_slugdiv_callback() {
        $options = get_option( 'twd_admin_hider_post_settings' );
        $status  = isset( $options['post_slugdiv'] ) ? checked( 1, $options['post_slugdiv'], false ) : '';

        $html    = '<input type="checkbox" id="post_slugdiv" name="twd_admin_hider_post_settings[post_slugdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function post_authordiv_callback() {
        $options = get_option( 'twd_admin_hider_post_settings' );
        $status  = isset( $options['post_authordiv'] ) ? checked( 1, $options['post_authordiv'], false ) : '';

        $html    = '<input type="checkbox" id="post_authordiv" name="twd_admin_hider_post_settings[post_authordiv]" value="1"' . $status . '/>';

        echo $html;
    }


/**
 * Meta Boxes - Page - Callbacks
 */

function twd_admin_hider_settings_section_page_callback() {
    echo '<p>Select which Page Meta Boxes you wish to hide.</p>';
}
    function page_postcustom_callback() {
        $options = get_option( 'twd_admin_hider_page_settings' );
        $status  = isset( $options['page_postcustom'] ) ? checked( 1, $options['page_postcustom'], false ) : '';

        $html    = '<input type="checkbox" id="page_postcustom" name="twd_admin_hider_page_settings[page_postcustom]" value="1"' . $status . '/>';

        echo $html;
    }
    function page_postexcerpt_callback() {
        $options = get_option( 'twd_admin_hider_page_settings' );
        $status  = isset( $options['page_postexcerpt'] ) ? checked( 1, $options['page_postexcerpt'], false ) : '';

        $html    = '<input type="checkbox" id="page_postexcerpt" name="twd_admin_hider_page_settings[page_postexcerpt]" value="1"' . $status . '/>';

        echo $html;
    }
    function page_commentstatusdiv_callback() {
        $options = get_option( 'twd_admin_hider_page_settings' );
        $status  = isset( $options['page_commentstatusdiv'] ) ? checked( 1, $options['page_commentstatusdiv'], false ) : '';

        $html    = '<input type="checkbox" id="page_commentstatusdiv" name="twd_admin_hider_page_settings[page_commentstatusdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function page_commentsdiv_callback() {
        $options = get_option( 'twd_admin_hider_page_settings' );
        $status  = isset( $options['page_commentsdiv'] ) ? checked( 1, $options['page_commentsdiv'], false ) : '';

        $html    = '<input type="checkbox" id="page_commentsdiv" name="twd_admin_hider_page_settings[page_commentsdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function page_trackbacksdiv_callback() {
        $options = get_option( 'twd_admin_hider_page_settings' );
        $status  = isset( $options['page_trackbacksdiv'] ) ? checked( 1, $options['page_trackbacksdiv'], false ) : '';

        $html    = '<input type="checkbox" id="page_trackbacksdiv" name="twd_admin_hider_page_settings[page_trackbacksdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function page_slugdiv_callback() {
        $options = get_option( 'twd_admin_hider_page_settings' );
        $status  = isset( $options['page_slugdiv'] ) ? checked( 1, $options['page_slugdiv'], false ) : '';

        $html    = '<input type="checkbox" id="page_slugdiv" name="twd_admin_hider_page_settings[page_slugdiv]" value="1"' . $status . '/>';

        echo $html;
    }
    function page_authordiv_callback() {
        $options = get_option( 'twd_admin_hider_page_settings' );
        $status  = isset( $options['page_authordiv'] ) ? checked( 1, $options['page_authordiv'], false ) : '';

        $html    = '<input type="checkbox" id="page_authordiv" name="twd_admin_hider_page_settings[page_authordiv]" value="1"' . $status . '/>';

        echo $html;
    }


/**
 * Menus - Callbacks
 */

function twd_admin_hider_settings_section_menus_callback() {
    echo '<p>Select which Menus you wish to hide.</p>';
}
    function menu_dashboard_updates_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_dashboard_updates'] ) ? checked( 1, $options['menu_dashboard_updates'], false ) : '';

        $html    = '<input type="checkbox" id="menu_dashboard_updates" name="twd_admin_hider_menu_settings[menu_dashboard_updates]"" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_posts_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_posts'] ) ? checked( 1, $options['menu_posts'], false ) : '';

        $html    = '<input type="checkbox" id="menu_posts" name="twd_admin_hider_menu_settings[menu_posts]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_media_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_media'] ) ? checked( 1, $options['menu_media'], false ) : '';

        $html    = '<input type="checkbox" id="menu_media" name="twd_admin_hider_menu_settings[menu_media]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_links_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_links'] ) ? checked( 1, $options['menu_links'], false ) : '';

        $html    = '<input type="checkbox" id="menu_links" name="twd_admin_hider_menu_settings[menu_links]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_comments_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_comments'] ) ? checked( 1, $options['menu_comments'], false ) : '';

        $html    = '<input type="checkbox" id="menu_comments" name="twd_admin_hider_menu_settings[menu_comments]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_pages_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_pages'] ) ? checked( 1, $options['menu_pages'], false ) : '';

        $html    = '<input type="checkbox" id="menu_pages" name="twd_admin_hider_menu_settings[menu_pages]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_plugins_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_plugins'] ) ? checked( 1, $options['menu_plugins'], false ) : '';

        $html    = '<input type="checkbox" id="menu_plugins" name="twd_admin_hider_menu_settings[menu_plugins]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_users_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_users'] ) ? checked( 1, $options['menu_users'], false ) : '';

        $html    = '<input type="checkbox" id="menu_users" name="twd_admin_hider_menu_settings[menu_users]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_tools_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_tools'] ) ? checked( 1, $options['menu_tools'], false ) : '';

        $html    = '<input type="checkbox" id="menu_tools" name="twd_admin_hider_menu_settings[menu_tools]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_appearance_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_appearance'] ) ? checked( 1, $options['menu_appearance'], false ) : '';

        $html    = '<input type="checkbox" id="menu_appearance" name="twd_admin_hider_menu_settings[menu_appearance]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_appearance_themes_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_appearance_themes'] ) ? checked( 1, $options['menu_appearance_themes'], false ) : '';

        $html    = '<input type="checkbox" id="menu_appearance_themes" name="twd_admin_hider_menu_settings[menu_appearance_themes]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_appearance_widgets_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_appearance_widgets'] ) ? checked( 1, $options['menu_appearance_widgets'], false ) : '';

        $html    = '<input type="checkbox" id="menu_appearance_widgets" name="twd_admin_hider_menu_settings[menu_appearance_widgets]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_appearance_editor_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_appearance_editor'] ) ? checked( 1, $options['menu_appearance_editor'], false ) : '';

        $html    = '<input type="checkbox" id="menu_appearance_editor" name="twd_admin_hider_menu_settings[menu_appearance_editor]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_settings_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_settings'] ) ? checked( 1, $options['menu_settings'], false ) : '';

        $html    = '<input type="checkbox" id="menu_settings" name="twd_admin_hider_menu_settings[menu_settings]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_settings_general_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_settings_general'] ) ? checked( 1, $options['menu_settings_general'], false ) : '';

        $html    = '<input type="checkbox" id="menu_settings_general" name="twd_admin_hider_menu_settings[menu_settings_general]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_settings_writing_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_settings_writing'] ) ? checked( 1, $options['menu_settings_writing'], false ) : '';

        $html    = '<input type="checkbox" id="menu_settings_writing" name="twd_admin_hider_menu_settings[menu_settings_writing]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_settings_reading_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_settings_reading'] ) ? checked( 1, $options['menu_settings_reading'], false ) : '';

        $html    = '<input type="checkbox" id="menu_settings_reading" name="twd_admin_hider_menu_settings[menu_settings_reading]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_settings_discussion_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_settings_discussion'] ) ? checked( 1, $options['menu_settings_discussion'], false ) : '';

        $html    = '<input type="checkbox" id="menu_settings_discussion" name="twd_admin_hider_menu_settings[menu_settings_discussion]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_settings_media_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_settings_media'] ) ? checked( 1, $options['menu_settings_media'], false ) : '';

        $html    = '<input type="checkbox" id="menu_settings_media" name="twd_admin_hider_menu_settings[menu_settings_media]" value="1"' . $status . '/>';

        echo $html;
    }
    function menu_settings_permalinks_callback() {
        $options = get_option( 'twd_admin_hider_menu_settings' );
        $status  = isset( $options['menu_settings_permalinks'] ) ? checked( 1, $options['menu_settings_permalinks'], false ) : '';

        $html    = '<input type="checkbox" id="menu_settings_permalinks" name="twd_admin_hider_menu_settings[menu_settings_permalinks]" value="1"' . $status . '/>';

        echo $html;
    }