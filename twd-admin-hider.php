<?php
/*
    Plugin Name: TWD Admin Hider
    Plugin URI: http://www.timwestdesigns.com/
    Description: Hides parts of the Admin from other users.
    Author: Tim West Designs
    Author URI: http://www.timwestdesigns.com/
    Version: 1.8.1
*/

/**
 * If this file is called directly, abort.
 */
if ( ! defined( 'WPINC' ) ) {
    die();
}


/**
 * Define useful Constants
 */
define( 'TWD_ADMIN_HIDER_DIR', plugin_dir_path(__FILE__) );
define( 'TWD_ADMIN_HIDER_URL', plugin_dir_url(__FILE__) );
define( 'TWD_ADMIN_HIDER_BASE', plugin_basename( __FILE__ ) );


/**
 * Actions to perform once on plugin activation go here
 */
function twd_admin_hider_activation() {
}
// register_activation_hook( __FILE__, 'twd_admin_hider_activation' );


/**
 * Actions to perform once on plugin deactivation go here
 */
function twd_admin_hider_deactivation() {
}
// register_deactivation_hook( __FILE__, 'twd_admin_hider_deactivation' );


/**
 * Add settings link on plugin page
 */
function twd_admin_hider_settings_link( $links ) {
    array_unshift( $links, '<a href="options-general.php?page=twd_admin_hider_settings">Settings</a>' );

    return $links;
}
add_filter( 'plugin_action_links_' . TWD_ADMIN_HIDER_BASE, 'twd_admin_hider_settings_link' );


require_once TWD_ADMIN_HIDER_DIR . '/includes/options.php';
require_once TWD_ADMIN_HIDER_DIR . '/includes/functions.php';